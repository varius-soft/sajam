@extends('layout')

@section('body')
	page page-template
@stop

@section('title')
	Kategorije
@stop

@section('site_title')
	Kategorije
@stop

@section('content')
	 <!--Banner-->
    <section>
        <div class="csi-banner csi-banner-inner">
            <div class="csi-banner-style">
                <div class="csi-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="csi-heading-area">
                                    <div class="csi-heading">
                                        <h2 class="title">{{ $category->name }}</h2>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li><a href="/"><i class="icon-home6"></i>Naslovna</a></li>
                                        <li class="active">{{ $category->name }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div><!--//.ROW-->
                    </div>
                </div>
                <!-- //.INNER -->
            </div>
        </div>
    </section>
    <!--Banner END-->


    <!--News-->
    <section>
        <div id="csi-news" class="csi-news">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        @foreach($stands as $stand)
                            @if($stand->status == 'APPROVED')
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="csi-single-news">
                                    <figure>
                                        <a href="/stand/{{$stand->id}}"><img style="width:400px; height: 300px;" src="{{asset('uploads')}}/{{$stand->id}}/{{$stand->image}}" alt=""></a>    
                                    </figure>
                                    <h3 class="title"><a href="/stand/{{$stand->id}}">{{ $stand->name }}</a></h3>
                                    <p><a href="/stand/{{$stand->id}}">Posetite štand</a></p>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section>
    <!--News END-->
@stop