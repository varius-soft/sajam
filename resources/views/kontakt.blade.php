@extends('layout')

@section('body')
	page page-template
@stop

@section('title')
	Kontakt
@stop

@section('site_title')
	Kontakt
@stop

@section('content')
	<!--Banner-->
    <section>
        <div class="csi-banner csi-banner-inner">
            <div class="csi-banner-style">
                <div class="csi-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="csi-heading-area">
                                    <div class="csi-heading">
                                        <h2 class="title">Kontakt</h2>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li><a href="/"><i class="icon-home6"></i>Naslovna</a></li>
                                        <li class="active">Kontakt</li>
                                    </ul>
                                </div>
                            </div>
                        </div><!--//.ROW-->
                    </div>
                </div>
                <!-- //.INNER -->
            </div>
        </div>
    </section>
    <!--Banner END-->





    <section>
        <div id="csi-contact" class="csi-contact">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-8">
                            <form method="POST"  action="/contact-main">
                                                @csrf
                                                

                                                <div class="form-group">
                                                    <input type="text" name="ime_prezime" class="form-control csiname" id="csiname" placeholder="Ime i Prezime" required>
                                                </div>

                                                <div class="form-group">
                                                    <input type="email" name="mail" class="form-control csiemail" id="csiemail" placeholder="E-mail" required>
                                                </div>

                                                <div class="form-group">
                                                    <input type="text" name="telefon" class="form-control csiemail" id="telefon" placeholder="Telefon" >
                                                </div>

                                                <div class="form-group">
                                                    <textarea class="form-control csimessage" name="poruka" id="poruka" rows="5" placeholder="Poruka" ></textarea>
                                                </div>
                                                <button type="submit" name="submit" value="contact-form" class="csi-btn hvr-glow hvr-radial-out csisend csi-send">Pošalji  <i class="fa fa-paper-plane"></i></button>
                                            </form>
                            <!-- MODAL SECTION -->
                            
                        </div> <!--//.COL-->
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section>
@stop