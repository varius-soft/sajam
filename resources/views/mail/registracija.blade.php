<!DOCTYPE html >
<html>

<body>

<!-- HEADER -->
<div class="container" style="padding-top:70px; padding-bottom: 70px;">

    <table>
        <tr>
            <td></td>
            <td class="header container">

                <div class="content">
                    <table >
                        <tr>
                            <td>

                            </td>

                        </tr>
                    </table>
                </div>

            </td>
            <td></td>
        </tr>
    </table><!-- /HEADER -->


    <!-- BODY -->
    <table class="body-wrap">
        <tr>
            <td></td>
            <td class="container">

                <div class="content">
                    <table>
                        <tr>
                            <td>


                                <!-- You may like to include a Hero Image -->
                                <p><img src="https://sajam.pfm.rs/assets/img/logo.png" style="width: 120px;height:auto;" /></p>
                                <!-- /Hero Image -->

                                <br/>
                                <h3>REGISTRACIJA NOVOG KORISNIKA <small>{{ date('d.m.Y - H:i', strtotime($vreme)) }}</small></h3>
                                <h4>{{$preduzece}} - {{$name}}</h4>
                                <br>
                       

                                <br/>


                                <hr>



                                <!-- address detals -->
                                <table class="columns" width="100%">
                                    <tr>
                                        <td>

                                            <!--- column 1 -->
                                            <table align="left" class="column">
                                                <tr>
                                                    <td>
                                                        <h5 class="">Podaci:</h5>
                                                        <p class="">
                                                            Ime i prezime: {{$name}}<br/>
                                                            Preduzeće: {{$preduzece}}<br/>
                                                            
                                                        </p>
                                                        </p>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- /column 1 -->




                                            <span class="clear"></span>

                                        </td>
                                    </tr>
                                </table>
                                <!-- /address details -->

                                <br/>

                                <p style="text-align:center;">
                                    <a class="btn" href="https://sajam.pfm.rs/">Pogledajte prijave &raquo;</a>
                                </p>

                                <br/>


                            </td>
                        </tr>
                    </table>
                </div>

            </td>
            <td></td>
        </tr>
    </table>
    <!-- /BODY -->
</div>

</body>
</html>