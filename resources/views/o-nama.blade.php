@extends('layout')

@section('body')
	page page-template
@stop

@section('title')
	O Organizatoru
@stop

@section('site_title')
	O Organizatoru
@stop

@section('content')
	 <!--Banner-->
    <section>
        <div class="csi-banner csi-banner-inner">
            <div class="csi-banner-style">
                <div class="csi-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="csi-heading-area">
                                    <div class="csi-heading">
                                        <h2 class="title">O Organizatoru</h2>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li><a href="/"><i class="icon-home6"></i>Naslovna</a></li>
                                        <li class="active">O Organizatoru</li>
                                    </ul>
                                </div>
                            </div>
                        </div><!--//.ROW-->
                    </div>
                </div>
                <!-- //.INNER -->
            </div>
        </div>
    </section>
    <!--Banner END-->




    <!--ABOUT-->
    <section>
        <div id="csi-about" class="csi-about">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-5">
                            <div class="csi-about-img">
                                <img src="assets/img/about.png" alt="about">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-7">
                            <div class="csi-about-content-area csi-about-content-area-left">
                                <div class="csi-heading">
                                    <h2 class="heading">Sajam omladinskog preduzetništva ’20</h2>
                                    <h3 class="subheading">O Organizatoru</h3>
                                </div>
                                <div class="csi-about-content">
                                    <p class="text">
                                        Privredni forum mladih je organizacija nastala okupljanjem motivisanih mladih ljudi zainteresovanih za aktivno rešavanje problema nezaposlenosti kod mladih. Kao dobrovoljna, nevladina, nestranačka, nepolitička i neprofitna organizacija, Forum teži da podstakne umrežavanje mladih, privrede, države i svih relevantnih činilaca, i time postane glavna platforma za razvoj omladinskog preduzetništva u Republici Srbiji.
                                    </p>
                                    </br>
                                    <p class="text">
                                        Od samog osnivanja krajem 2014. godine, pa do sada, uspešno je realizovano 5 projekata, 2 sajma, 25 edukativnih događaja u 19 gradova širom Srbije, izrađena su 4 strateška dokumenta i snimljen je dokumentarni film o razvojnom putu mladih preduzetnika koji svoj biznis pokreću i vode u Srbiji.
                                    </p>
                                    </br>
                                    <p class="text">
                                        Kroz ove aktivnosti, ostvarena je saradnja sa preko 30 istaknutih kompanija, preko 20 domaćih i inostranih institucija i visokih predstavnika, 200 preduzetnika i više od 7000 mladih.
                                    </p>
                                    </br>
                                    <p class="text">
                                        Aktivnosti organizacije Privredni forum mladih su u dva navrata (2017. i 2019. godine) ocenjene kao Najbolji nacionalni projekti koji promovišu preduzetništvo u zemlji od strane Evropske komisije. Prošle godine smo imali tu čast da projekat (Privrednog foruma mladih i Ministarstva omladine i sporta) Put mladog preduzetnika 2018. bude predstavljen na Evropskom skupu posvećenom MSP sektoru i preduzetništvu u Talinu (Estoniji) i da uđe u veliko finale (u tri najbolja evropska nacionalna projekta zajedno sa Belgijom i Finskom).
                                    </p>   
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section>
    <!--ABOUT END-->





       <!--SPEAKERS
    <section>
        <div id="csi-speakers" class="csi-speakers">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="csi-heading">
                                <h2 class="heading">Organizatori </h2>
                            </div>
                        </div>
                    </div>
                
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="csi-single-speaker">
                                <figure>
                                    <a class="profile-img"  ><img src="assets/img/org1.jpg" alt="speaker"/></a>
                                    <figcaption>
                                        <div class="social-group">
                                            <a class="sp-in" target="_blank" href="https://www.linkedin.com/in/marijanamarinkovic/"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                        <div class="speaker-info">
                                            <h3 class="title"><a  >Marijana Marinković</a></h3>
                                            <h4 class="subtitle">Predsednica Udruženja Privredni forum mladih</h4>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="csi-single-speaker">
                                <figure>
                                    <a class="profile-img"  ><img src="assets/img/org2.jpg" alt="speaker"/></a>
                                    <figcaption>
                                        <div class="social-group">
                                            <a class="sp-in" target="_blank"  href="https://www.linkedin.com/in/jasna-slamarski/"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                        <div class="speaker-info">
                                            <h3 class="title"><a  >Jasna Slamarski</a></h3>
                                            <h4 class="subtitle">Koordinator Sajma</h4>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="csi-single-speaker">
                                <figure>
                                    <a class="profile-img"  ><img src="assets/img/org3.jpg" alt="speaker"/></a>
                                    <figcaption>
                                        <div class="social-group">
                                            <a class="sp-in" target="_blank"  href="https://www.facebook.com/nebojsa.knezevic.3"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                        <div class="speaker-info">
                                            <h3 class="title"><a  >Nebojša Knežević</a></h3>
                                            <h4 class="subtitle">Organizacioni tim</h4>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="csi-single-speaker">
                                <figure>
                                    <a class="profile-img"  ><img src="assets/img/org4.jpg" alt="speaker"/></a>
                                    <figcaption>
                                        <div class="social-group">
                                            <a class="sp-in" target="_blank"  href="https://www.linkedin.com/in/nikola-opacic-605a34143/"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                        <div class="speaker-info">
                                            <h3 class="title"><a  >Nikola Opačič</a></h3>
                                            <h4 class="subtitle">Organizacioni tim</h4>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                    
                </div>
        
            </div>
            
        </div>
    </section>
    SPEAKERS END-->
@stop