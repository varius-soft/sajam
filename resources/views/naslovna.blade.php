@extends('layout')

@section('body')
	home
@stop

@section('title')
	Naslovna
@stop

@section('site_title')
	Naslovna
@stop

@section('content')
	<!--Banner-->
    <section>
        <div class="csi-banner">
            <div class="csi-banner-style">
                <div class="csi-inner">
                    <div class="container">
                        <div class="csi-banner-content">
                            <h3 class="csi-subtitle">Sajam omladinskog preduzetništva ’20</h3>
                            
                            <p class="date"><span>8.12.2020. </span> </p>
                        </div>
                    </div>
                    <!-- //.container -->
                </div>
                <!-- //.INNER -->
            </div>
        </div>
    </section>
    <!--Banner END-->




    <!--ABOUT-->
    <section>
        <div id="csi-about" class="csi-about">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-5">
                            <div class="csi-about-img">
                                <img src="assets/img/about.png" alt="about">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-7">
                            <div class="csi-about-content-area">
                                <div class="csi-heading">
                                    <h2 class="heading">Sajam omladinskog preduzetništva ’20</h2>
                                    <h3 class="subheading">O Sajmu</h3>
                                </div>
                                <div class="csi-about-content">
                                    <p class="text">
                                       <strong> Privredni forum mladih u saradnji sa Ministarstvom omladine i sporta </strong>organizuje projekat pod nazivom „Mladi i privreda – Put mladog preduzetnika“. Sajam omladinskog preduzetništva već dve godine unazad okuplja najznačajnije predstavnike preduzetničke zajednice naše zemlje i  predstavlja događaj koji je do sada posetilo preko 3000 mladih i gde je oko 200 preduzetnika imalo priliku da se predstavi posetiocima.
                                    </p>
                                    </br>
                                    <p class="text">
                                        Ovogodišnji sajam, osim što je prvi i jedini sajam posvećen mladim preduzetnicima, biće prvi i trenutno jedini sajam omladinskog preduzetništva u regionu u <strong>online formatu</strong>. Cilj ovogodišnjeg sajma je da doprinese povećanju stepena zapošljivosti mladih u Republici Srbiji stvaranjem stimulativnijeg ambijenta za rad, zapošljavanje, samozapošljavanje i aktivizam kod mladih kroz promociju dobre prakse u 4 statistička regiona širom Republike Srbije (Vojvodina, Beograd, Šumadija i Zapadna Srbija i Južna i Istočna Srbija).
                                    </p>
                                    </br>
                                    <p class="text">
                                        Stoga će nam se na ovogodišnjem sajmu predstaviti preduzetnici iz gore pomenutih regiona, te će posetioci imati priliku da se bolje upoznaju sa primerima dobre prakse mladih preduzetničkih poduhvata iz cele Srbije.
                                    </p>
                                    </br>
                                    <p class="text">
                                        Uz već dobro poznato partnerstvo, koje traje godinama unazad koje Privredni forum mladih ima sa Ministarstvom omladine i sporta, na ovogodišnjem projektu podršku su nam dali, za šta smo veoma ponosni, i <strong>Udruženje mladih privrednika Srbije (UMPS) i Institut za razvoj i inovacije (IRI)</strong>. Upravo sa UMPS i IRI, izradili smo predlog dokumenta kojim bi se definisao i prepoznao termin „mladi preduzetnik“, kao i dokument kojim ćemo predstaviti privredne potencijale za razvoj omladinskog preduzetništva i otpočinjanje posla u 4 statistička regiona Republike Srbije.
                                    </p>   
                                    </br>
                                    <p class="text">
                                        Pozivamo te da budeš deo Sajma omladinskog preduzetništva ’20 i podržiš preduzetničke inicijative mladih ljudi iz cele Srbije!
                                    </p>
                                    </br>
                                    <p class="text">
                                        Registruj se i uzmi učešće 
                                    </p>
                                    <div class="btn-area">
                                        <a class="csi-btn csi-scroll" href="/registracija">Registruj se </a>
                                        <a class="csi-btn csi-btn-brand" href="/agenda">Program Sajma </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section>
    <!--ABOUT END-->


    <!--SCHEDULE-->
    <section>
        <div id="csi-schedule" class="csi-schedule">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="csi-heading csi-heading-white">
                                <h2 class="heading">Kategorije </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="csi-tab">
                                
                                <ul class="nav nav-pills csi-nav">
                                    @foreach($categories as $c)
                                    <li @if($c->id == 1) class="active" @endif><a data-toggle="pill" href="#category{{ $c->id }}"><h3>{{ $c->name }} </h3></a></li>
                                    @endforeach
                                </ul>

                                <div class="tab-content csi-tab-content">

                                    @foreach($categories as $c)
                                    <div id="category{{ $c->id }}" class="tab-pane fade  {{ ($c->id==1) ? "active in" : "" }}">
                                        <div class="panel-group" id="accordion{{ $c->id }}" role="tablist" aria-multiselectable="true">
                                            @foreach($c->stands as $s)
                                                        @if($s->status == 'APPROVED')
                                                        <div class="panel panel-default csi-panel">
                                                            <div class="panel-heading" role="tab" id="heading{{$loop->index}}">
                                                                <div class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion{{ $c->id }}" href="#collapse{{$loop->index}}" aria-expanded="true" aria-controls="collapse{{$loop->index}}">
                                                                        <div class="csi-single-schedule">
                                                                            <div class="author">
                                                                                <img style="width: 100px; height: 100px;" src="{{asset('uploads')}}/{{$s->id}}/{{$s->image}}" alt="{{$s->name}}"/>
                                                                            </div>
                                                                            <div class="schedule-info">
                                                                                <h3 class="title">{{ $s->name }}</h3>
                                                                                <h4 class="author-info"><span>{{ $s->email }}</span></h4>
                                                                                <h4><a href="/stand/{{ $s->id }}">Posetite štand</a></h4>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div id="collapse{{$loop->index}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$loop->index}}">
                                                                <div class="panel-body">
                                                                    <p class="text">
                                                                        {{ $s->short_description }}
                                                                    </p>
                                                                    <h4 class="location"><strong>{{ $s->site }}</strong>    </h4>

                                                                    <div class="csi-share">
                                                                        <ul class="list-inline csi-social">
                                                                            <li><a target="_blank" href="{{$s->facebook}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                                            <li><a target="_blank" href="{{$s->twiter}}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                                            <li><a target="_blank" href="{{$s->instagram}}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                                                            <li><a target="_blank" href="{{$s->linkedin}}"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                                                                            <li><a target="_blank" href="skype:{{$s->skype}}?userinfo"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>

                                                        </div>
                                                        @endif
                                             @endforeach 
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//.ROW-->
                </div>
                <!-- //.CONTAINER -->
            </div>
            <!-- //.INNER -->
        </div>
    </section>
    <!--SCHEDULE END-->


    <!--SPEAKERS
    <section>
        <div id="csi-speakers" class="csi-speakers">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="csi-heading">
                                <h2 class="heading">Organizatori </h2>
                            </div>
                        </div>
                    </div>
                  
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="csi-single-speaker">
                                <figure>
                                    <a class="profile-img"  ><img src="assets/img/org1.jpg" alt="speaker"/></a>
                                    <figcaption>
                                        <div class="social-group">
                                            <a class="sp-in" target="_blank"  href="https://www.linkedin.com/in/marijanamarinkovic/"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                        <div class="speaker-info">
                                            <h3 class="title"><a  >Marijana Marinković</a></h3>
                                            <h4 class="subtitle">Predsednica Udruženja Privredni forum mladih</h4>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="csi-single-speaker">
                                <figure>
                                    <a class="profile-img"  ><img src="assets/img/org2.jpg" alt="speaker"/></a>
                                    <figcaption>
                                        <div class="social-group">
                                            <a class="sp-in" target="_blank" href="https://www.linkedin.com/in/jasna-slamarski/"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                        <div class="speaker-info">
                                            <h3 class="title"><a  >Jasna Slamarski</a></h3>
                                            <h4 class="subtitle">Koordinator Sajma</h4>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="csi-single-speaker">
                                <figure>
                                    <a class="profile-img"  ><img src="assets/img/org3.jpg" alt="speaker"/></a>
                                    <figcaption>
                                        <div class="social-group">
                                            <a class="sp-in" target="_blank"  href="https://www.facebook.com/nebojsa.knezevic.3"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                        <div class="speaker-info">
                                            <h3 class="title"><a  >Nebojša Knežević</a></h3>
                                            <h4 class="subtitle">Organizacioni tim</h4>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="csi-single-speaker">
                                <figure>
                                    <a class="profile-img"  ><img src="assets/img/org4.jpg" alt="speaker"/></a>
                                    <figcaption>
                                        <div class="social-group">
                                            <a class="sp-in" target="_blank"   href="https://www.linkedin.com/in/nikola-opacic-605a34143/"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                        <div class="speaker-info">
                                            <h3 class="title"><a  >Nikola Opačič</a></h3>
                                            <h4 class="subtitle">Organizacioni tim</h4>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                   
                </div>
              
            </div>
        
        </div>
    </section>
    SPEAKERS END-->


    <!--VIDEO-->
    <section>
        <div id="csi-video" class="csi-video">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="csi-video-area">
                                <figure>
                                    <figcaption>
                                        <div class="video-icon">
                                            <div class="csi-vertical">
                                                <a id="myModalLabel" class="icon" href="#" data-toggle="modal" data-target="#csi-modal">
                                                    <i class="fa fa-play " aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                                <!-- Modal-->
                                <div id="csi-modal" class="modal fade csi-modal">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <iframe id="modalvideo" src="https://www.youtube.com/embed/q_fv6rOfcgU" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- //.Modal-->
                            </div>
                        </div>
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section>
    <!--//.VIDEO END-->


    <!--SPONSORED-->
    <section>
        <div id="csi-sponsors" class="csi-sponsors">
            <div class="csi-inner-bg">
                <div class="csi-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="csi-heading">
                                    <h2 class="heading">Partneri</h2>
                                </div>
                            </div>
                        </div>
                        <!--//main row-->
                        <div class="row">
                            <div class="col-xs-12">
                                <h3 class="sponsored-heading first-heading">Zlatni partner</h3>
                                <div class="sponsors-area">
                                    <div class="single">
                                        <a class="" href="#"><img src="assets/img/mos.png" alt=""/></a>
                                    </div>
                                </div>
                            </div>
                            <!--//col-->
                        </div>
                        <!--//row-->
                        <div class="row">
                            <div class="col-xs-12">
                                <h3 class="sponsored-heading">Partneri saradnici</h3>
                                <div class="sponsors-area">
                                    <div class="single">
                                        <a class="" href="#"><img src="assets/img/umps.png" alt=""/></a>
                                    </div>
                                    <div class="single">
                                        <a class="" href="#"><img src="assets/img/iri.png" alt=""/></a>
                                    </div>
                                </div>
                            </div>
                            <!--//col-->
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <h3 class="sponsored-heading">Edukativni partner</h3>
                                <div class="sponsors-area">
                                    <div class="single">
                                        <a class="" href="#"><img src="assets/img/mef.png" alt=""/></a>
                                    </div>
                                </div>
                            </div>
                            <!--//col-->
                        </div>
                    </div>
                    <!--//container-->
                </div>
            </div>
            <!--//csi-inner-->
        </div>
    </section>
    <!--SPONSORED END-->

@stop