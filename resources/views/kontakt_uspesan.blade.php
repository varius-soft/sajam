@extends('layout')

@section('body')
	page page-template
@stop

@section('title')
	KONTAKT USPEŠAN
@stop

@section('site_title')
	KONTAKT USPEŠAN
@stop

@section('content')
	 <!--Banner-->
    <section>
        <div class="csi-banner csi-banner-inner">
            <div class="csi-banner-style">
                <div class="csi-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="csi-heading-area">
                                    <div class="csi-heading">
                                        <h2 class="title">KONTAKT USPEŠAN</h2>
                                    </div>
                                    
                                </div>
                            </div>
                        </div><!--//.ROW-->
                    </div>
                </div>
                <!-- //.INNER -->
            </div>
        </div>
    </section>
    <!--Banner END-->




    <!--ABOUT-->
    <section>
        <div id="csi-about" class="csi-about">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-5">
                            <div class="csi-about-img">
                                <img src="assets/img/about.png" alt="about">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-7">
                            <div class="csi-about-content-area csi-about-content-area-left">
                                <div class="csi-heading">
                                    <h2 class="heading">KONTAKT USPEŠAN</h2>
                                </div>
                                <div class="csi-about-content">
                                    <p class="text">
                                        Uspešno ste nas kontaktirali. Potrudićemo se da Vam odgovormo u najkraćem mogućem roku. 
                                        <br><br>
                                        Hvala.
                                    </p>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section>
    <!--ABOUT END-->

@stop