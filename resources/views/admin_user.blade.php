@extends('layout')

@section('body')
	page page-template
@stop

@section('title')
	Registracija ADMIN
@stop

@section('site_title')
	Registracija ADMIN
@stop

@section('content')
	<!--Banner-->
    <section>
        <div class="csi-banner csi-banner-inner">
            <div class="csi-banner-style">
                <div class="csi-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="csi-heading-area">
                                    <div class="csi-heading">
                                        <h2 class="title">Registracija ADMIN</h2>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li><a href="/"><i class="icon-home6"></i>Naslovna</a></li>
                                        <li class="active">Registracija</li>
                                    </ul>
                                </div>
                            </div>
                        </div><!--//.ROW-->
                    </div>
                </div>
                <!-- //.INNER -->
            </div>
        </div>
    </section>
    <!--Banner END-->





    <section>
        <div id="csi-contact" class="csi-contact">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-8">
                            <form method="POST" class="" action="/crt-admin-user-function">
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control csiname" id="name" placeholder="Ime i Prezime *" value="{{ Request::old('name') }}" required>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control csiname" id="password" placeholder="Lozinka *" value="{{ Request::old('password') }}" required>
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control csiemail" id="email" placeholder="Email adresa *" value="{{ Request::old('email') }}" required>
                                </div>
                            
  
                                <button type="submit" name="submit" value="contact-form" class="csi-btn hvr-glow hvr-radial-out csisend csi-send">Registruj ADMINA </button>
                                
                                <input type="hidden" value="{{ Session::token() }}" name="_token">
                        
                            </form>
                            <!-- MODAL SECTION -->
                            <div id="csi-form-modal" class="modal fade csi-form-modal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content csi-modal-content">
                                        <div class="modal-header csi-modal-header">
                                            <button type="button" class="close brand-color-hover" data-dismiss="modal" aria-label="Close">
                                                <i class="fa fa-power-off"></i>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="alert csi-form-msg" role="alert"></div>
                                        </div> <!--//MODAL BODY-->
                                    </div>
                                </div>
                            </div> <!-- //MODAL -->
                        </div> <!--//.COL-->
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section>
@stop