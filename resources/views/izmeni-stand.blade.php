@extends('layout')

@section('body')
	page page-template
@stop

@section('title')
	{{ $stand->name }}
@stop

@section('site_title')
	{{ $stand->name }}
@stop

@section('content')
	<!--Banner-->
    <section>
        <div class="csi-banner csi-banner-inner">
            <div class="csi-banner-style">
                <div class="csi-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="csi-heading-area">
                                    <div class="csi-heading">
                                        <h2 class="title">{{ $stand->name }}</h2>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li><a href="/"><i class="icon-home6"></i>Naslovna</a></li>
                                        <li class="active">{{ $stand->name }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div><!--//.ROW-->
                    </div>
                </div>
                <!-- //.INNER -->
            </div>
        </div>
    </section>
    <!--Banner END-->

    <section>
        <div id="csi-contact" class="csi-contact">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-8">
                             @if($stand->status == 'APPROVED')
                            <H1>ŠTAND JE ODOBREN.</H1>    
                            @endif
                            @if($stand->status == 'NEW')
                            <H1>ŠTAND ČEKA NA ODOBRENJE.</H1>
                            @endif
                            <form method="POST"  action="/update-user" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                 <input type="text" name="id" id="id"  value="{{ $stand->id_user }}" hidden="">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control csiname" id="name" placeholder="Ime i Prezime *" value="{{ $user->name }}" required>
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control csiemail" id="email" placeholder="Email adresa *" value="{{ $stand->email }}" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="preduzece" class="form-control csiname" id="preduzece" placeholder="Pun naziv preduzeća *" value="{{ $stand->name }}" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="skraceno" class="form-control csiname" id="skraceno" placeholder="Skraćen naziv preduzeća" value="{{ $stand->short_name }}">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="pib" class="form-control csiname" id="pib" placeholder="Matični broj preduzeća *" value="{{ $stand->pib }}" required>
                                </div>
                                <div class="form-group">
                                    <select class="form-control csiname" name="kategorija" id="kategorija" required>
                                        @foreach($categories as $c)
                                            <option @if($c->id == $stand->id_category) selected @endif value="{{ $c->id }}">{{ $c->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!--  NOVO -->
                                <div class="form-group">
                                    <input type="text" name="telefon" class="form-control csiname" id="telefon" placeholder="Telefon" value="{{ $stand->phone }}">
                                </div>

                                <div class="form-group">
                                    <input type="text" name="sajt" class="form-control csiname" id="sajt" placeholder="Web sajt ( FORMAT: www.sajt.com ili sajt.com )" value="{{ $stand->site }}">
                                </div>

                               
<!--
                                <div class="form-group">
                                    <input type="url" name="ss" class="form-control csiname" id="ss" placeholder="Skype USERNAME" value="{{ $stand->skype }}">
                                </div>
-->
                                <div class="form-group">
                                    <input type="text" name="facebook" class="form-control csiname" id="facebook" placeholder="Facebook" value="{{ $stand->facebook }}">
                                </div>

                                <div class="form-group">
                                    <input type="text" name="instagram" class="form-control csiname" id="instagram" placeholder="Instagram" value="{{ $stand->instagram }}">
                                </div>

                                <div class="form-group">
                                    <input type="text" name="linkedin" class="form-control csiname" id="linkedin" placeholder="Linkedin" value="{{ $stand->linkedin }}">
                                </div>
<!--
                                <div class="form-group">
                                    <input type="url" name="tt" class="form-control csiname" id="tt" placeholder="Twitter" value="{{ $stand->twiter }}">
                                </div>
-->                             


                            
                               

                                <div class="form-group">
                                    <textarea  name="description" class="form-control csiname" id="description" placeholder="Opis" >
                                        {{ $stand->description }}
                                    </textarea>
                                </div>

                                <div class="form-group">
                                    <p><strong>NASLOVNA FOTOGRAFIJA (FORMAT 800 x 600 px)</strong></p>
                                    <input type="file" id="slika" name="slika" accept="image/png, image/jpeg, image/jpg ">
                                </div>
                            
                                <div class="form-group">
                                    <p><strong>PREZENTACIJA/KATALOG (PDF, PowerPoint, Word)</strong></p>
                                    <input type="file" id="dokument" name="dokument">
                                </div>
                                <!--  NOVO -->
                                <button type="submit" name="submit" value="contact-form" class="csi-btn hvr-glow hvr-radial-out csisend csi-send">Promeni </button>
                                
                               
                        
                            </form>
                            <!-- MODAL SECTION -->
                           
                        </div> <!--//.COL-->
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section>
@stop