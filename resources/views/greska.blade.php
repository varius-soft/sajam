@extends('layout')

@section('body')
	page page-template
@stop

@section('title')
	GREŠKA
@stop

@section('site_title')
	GREŠKA
@stop

@section('content')
	 <!--Banner-->
    <section>
        <div class="csi-banner csi-banner-inner">
            <div class="csi-banner-style">
                <div class="csi-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="csi-heading-area">
                                    <div class="csi-heading">
                                        <h2 class="title">GREŠKA</h2>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li><a href="/"><i class="icon-home6"></i>Naslovna</a></li>
                                        <li class="active">GREŠKA</li>
                                    </ul>
                                </div>
                            </div>
                        </div><!--//.ROW-->
                    </div>
                </div>
                <!-- //.INNER -->
            </div>
        </div>
    </section>
    <!--Banner END-->




    <!--ABOUT-->
    <section>
        <div id="csi-about" class="csi-about">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-5">
                            <div class="csi-about-img">
                                <img src="{{asset('assets/img/about.png')}}" alt="about">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-7">
                            <div class="csi-about-content-area csi-about-content-area-left">
                                <div class="csi-heading">
                                    <h2 class="heading">Stranica ne postoji ili je došlo do greške.</h2>
                                    <h3 class="subheading"><a href="/">Nazad na naslovnu stranu</a></h3>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section>
    <!--ABOUT END-->

@stop