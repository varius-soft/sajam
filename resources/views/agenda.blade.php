@extends('layout')

@section('body')
	page page-template
@stop

@section('title')
	Program Sajma
@stop

@section('site_title')
	Program Sajma
@stop

@section('content')
 <!--Banner-->
    <section>
        <div class="csi-banner csi-banner-inner">
            <div class="csi-banner-style">
                <div class="csi-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="csi-heading-area">
                                    <div class="csi-heading">
                                        <h2 class="title">Program Sajma</h2>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li><a href="/"><i class="icon-home6"></i>Naslovna</a></li>
                                        <li class="active">Program Sajma</li>
                                    </ul>
                                </div>
                            </div>
                        </div><!--//.ROW-->
                    </div>
                </div>
                <!-- //.INNER -->
            </div>
        </div>
    </section>
    <!--Banner END-->




    <!--SCHEDULE-->
    <section>
        <div id="csi-schedule" class="csi-schedule">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="csi-tab">
                                <ul class="nav nav-pills csi-nav">
                                    <li class="active"><a data-toggle="pill" href="#home"><h3>Agenda</h3> <p><span>utorak, </span>8.12.2020.
                                        
                                    </p></a>
                                </li>   
                                </ul>
                                <div class="tab-content csi-tab-content">
                                    <div id="home" class="tab-pane fade in active">
                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                            <div class="panel panel-default csi-panel">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <div class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                            <div class="csi-single-schedule">
                                                                <div class="schedule-info">
                                                                    <h4 class="time">11:00  - 11:15 </h4>
                                                                    <h3 class="title">Uvodno obraćanje</h3>
                                                                </div>
                                                                 
                                    
                                                
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <A href="https://meet.google.com/yfy-frvm-ckp" target="_blank" type="submit" name="submit" value="contact-form" class="csi-btn hvr-glow hvr-radial-out csisend csi-send">  <i class="fa fa-users"></i> PRIDRUŽI SE </A>
                                                </div>
                                            </div>
                                            <div class="panel panel-default csi-panel">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <div class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                            <div class="csi-single-schedule">
                                                                <div class="schedule-info">
                                                                    <h4 class="time">11:15  - 11:30 </h4>
                                                                    <h3 class="title">Predstavljanje predlog dokumenta – Definisanje termina "mladi preduzetnik"</h3>
                                                                </div>
                                                                
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <A href="https://meet.google.com/yfy-frvm-ckp" target="_blank" type="submit" name="submit" value="contact-form" class="csi-btn hvr-glow hvr-radial-out csisend csi-send">  <i class="fa fa-users"></i> PRIDRUŽI SE </A>
                                                </div>
                                            </div>
                                            <div class="panel panel-default csi-panel">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <div class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                            <div class="csi-single-schedule">
                                                                <div class="schedule-info">
                                                                    <h4 class="time">11:30  - 11:45 </h4>
                                                                    <h3 class="title">Predstavljanje dokumenta – Idustrijski potencijali za razvoj omladinskog preduzetništva i pokretanje posla u 4 statistička regiona Srbije</h3>
                                                                </div>
                                                                
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <A href="https://meet.google.com/yfy-frvm-ckp" target="_blank" type="submit" name="submit" value="contact-form" class="csi-btn hvr-glow hvr-radial-out csisend csi-send">  <i class="fa fa-users"></i> PRIDRUŽI SE </A>
                                                </div>
                                            </div>
                                            <div class="panel panel-default csi-panel">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <div class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                            <div class="csi-single-schedule">
                                                                <div class="schedule-info">
                                                                    <h4 class="time">11:45  - 12:45 </h4>
                                                                    <h3 class="title">Panel "Mladi i privreda"</h3>
                                                                </div>
                                                                
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <A href="https://meet.google.com/yfy-frvm-ckp" target="_blank" type="submit" name="submit" value="contact-form" class="csi-btn hvr-glow hvr-radial-out csisend csi-send">  <i class="fa fa-users"></i> PRIDRUŽI SE </A>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default csi-panel">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <div class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                            <div class="csi-single-schedule">
                                                                <div class="schedule-info">
                                                                    <h4 class="time">12:45  - 13:00 </h4>
                                                                    <h3 class="title">Pitanja</h3>
                                                                </div>
                                                               
                                                            </div>
                                                        </a>
                                                    </div>
                                                     <A href="https://meet.google.com/yfy-frvm-ckp" target="_blank" type="submit" name="submit" value="contact-form" class="csi-btn hvr-glow hvr-radial-out csisend csi-send">  <i class="fa fa-users"></i> PRIDRUŽI SE </A>
                                                </div>
                                            </div>
                                            <div class="panel panel-default csi-panel">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <div class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                            <div class="csi-single-schedule">
                                                                <div class="schedule-info">
                                                                    <h4 class="time">13:00 </h4>
                                                                    <h3 class="title">Otvaranje Sajma</h3>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default csi-panel">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <div class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                            <div class="csi-single-schedule">
                                                                <div class="schedule-info">
                                                                    <h4 class="time">14:00  - 16:30  </h4>
                                                                    <h3 class="title">Predstavljanje programa podrške za mlade preduzetnike i početnike u poslovanju</h3>
                                                                </div>
                                                               
                                                            </div>
                                                        </a>
                                                    </div>
                                                     <A href="https://meet.google.com/hzj-oppf-ouz" target="_blank" type="submit" name="submit" value="contact-form" class="csi-btn hvr-glow hvr-radial-out csisend csi-send">  <i class="fa fa-users"></i> PRIDRUŽI SE </A>
                                                </div>
                                            </div>
                                            <div class="panel panel-default csi-panel">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <div class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                            <div class="csi-single-schedule">
                                                                <div class="schedule-info">
                                                                    <h4 class="time">15:00  - 17:00 </h4>
                                                                    <h3 class="title">Inspirativne priče mladih preduzetnika</h3>
                                                                </div>
                                                                
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <A href="https://meet.google.com/pub-hrnp-rgn" target="_blank" type="submit" name="submit" value="contact-form" class="csi-btn hvr-glow hvr-radial-out csisend csi-send">  <i class="fa fa-users"></i> PRIDRUŽI SE </A>
                                                </div>
                                            </div>
                                            <div class="panel panel-default csi-panel">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <div class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                            <div class="csi-single-schedule">
                                                                <div class="schedule-info">
                                                                    <h4 class="time">19:00 </h4>
                                                                    <h3 class="title">Zatvaranje prvog dana Sajma</h3>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//.ROW-->
                </div>
                <!-- //.CONTAINER -->
            </div>
            <!-- //.INNER -->
        </div>
    </section>
    <!--SCHEDULE END-->
@stop