@extends('layout')

@section('body')
	page page-template
@stop

@section('title')
    {{ $stand->name }}
@stop

@section('site_title')
	{{ $stand->name }}
@stop

@section('content')
    
	 <!--Banner-->
    <section>
        <div class="csi-banner csi-banner-inner">
            <div class="csi-banner-style">
                <div class="csi-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="csi-heading-area">
                                    <div class="csi-heading">
                                        <h2 class="title">{{ $stand->name }}</h2>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li><a href="/"><i class="icon-home6"></i>Naslovna</a></li>
                                        <li><a href="/kategorija/{{ $category->id }}"><i class="icon-home6"></i>{{ $category->name }}</a></li>
                                        <li class="active">{{ $stand->name }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div><!--//.ROW-->
                    </div>
                </div>
                <!-- //.INNER -->
            </div>
        </div>
    </section>
    <!--Banner END-->
    @if($stand->status == 'APPROVED' || $admin == 1)

    <!--NEWS-->
    <section>
        <div id="csi-news" class="csi-news csi-news-singlepage">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <article>
                                <header>

                                    @if($stand->status == 'APPROVED' && $admin == 1)
                                    <H1>ŠTAND JE ODOBREN.</H1>
                                    <P>OVA SEKCIJE JE VIDLJIVA SAMO ADMINISTRATORIMA.</P>
                                    <A href="/vrati-zehtevi/{{$stand->id}}" type="submit" name="submit" value="contact-form" class="csi-btn hvr-glow hvr-radial-out csisend csi-send">VRATI U ZAHTEVE  <i class="fa fa-check"></i></A>
                                        <A href="/odbijenje/{{$stand->id}}" type="submit" name="submit" value="contact-form" class="csi-btn hvr-glow hvr-radial-out csisend csi-send">OBRIŠI  <i class="fa fa-times"></i></A>
                                    @endif

                                    @if($stand->status == 'NEW' && $admin == 1)
                                    <H1>ŠTAND ČEKA NA ODOBRENJE.</H1>
                                    <P>OVA SEKCIJE JE VIDLJIVA SAMO ADMINISTRATORIMA.</P>
                                        <A href="/odobri/{{$stand->id}}" type="submit" name="submit" value="contact-form" class="csi-btn hvr-glow hvr-radial-out csisend csi-send">ODOBRI  <i class="fa fa-check"></i></A>
                                        <A href="/odbijenje/{{$stand->id}}" type="submit" name="submit" value="contact-form" class="csi-btn hvr-glow hvr-radial-out csisend csi-send">OBRIŠI  <i class="fa fa-times"></i></A>
                                    @endif

                                    @if($stand->image)
                                    <figure>
                                        <img src="{{asset('uploads')}}/{{$stand->id}}/{{$stand->image}}" alt="New"/>
                                    </figure>
                                    @endif
                                    <div class="text-area">


                                        


                                        <h1 class="title">{{ $stand->name }}</h1>
                                        <div class="hits-area">
                                            <div class="date">
                                                @if($stand->phone)
                                                <a href="tel:{{$stand->phone}}"><i class="fa fa-phone" aria-hidden="true"></i> {{ $stand->phone }}</a>
                                                @endif
                                                <a target="_blank" href="https://{{ $stand->site }}"><i class="fa fa-laptop" aria-hidden="true"></i> {{ $stand->site }}</a>
                                                <a href="mailto:{{$stand->email}}"><i class="fa fa-envelope" aria-hidden="true"></i> {{ $stand->email }}</a>
                                                @if($stand->document)
                                                 <a target="_blank" href="{{asset('uploads')}}/{{$stand->id}}/{{$stand->document}}"><i class="fa fa-file" aria-hidden="true"></i> PREUZMITE DOKUMENT</a>
                                                @endif
                                            </div>
                                        </div>



                                    </div>
                                </header>
                                <section>
                                    <p>
										{!! $stand->description !!}
                                    </p>       
                                </section>
                                @if($stand->period && $stand->meeting)
                                <section>
                                        <P>SA IZLAGAČIMA NA ŠTANDU RAZGOVARAJ UŽIVO U PERIODU: {{$stand->period}}</P>
                                        <A href="{{$stand->meeting}}" target="_blank" type="text" name="submit" value="contact-form" class="csi-btn hvr-glow hvr-radial-out csisend csi-send">RAZGOVARAJ UŽIVO  <i class="fa fa-users"></i></A>
                                        

                                </section>
                                @endif
                                <footer>
                                    <div class="row">

                                        <div class="col-sm-offset-2 col-sm-8">
                                            <h1 class="title">KONTAKTIRAJTE NAS</h1>
                                            <div class="csi-share">
                                                <ul class="list-inline csi-social">
                                                    @if($stand->facebook)
                                                    <li><a target="_blank" href="{{$stand->facebook}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                    @endif
                                                    @if($stand->twiter)
                                                    <li><a target="_blank" href="{{$stand->twiter}}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                    @endif
                                                    @if($stand->instagram)
                                                    <li><a target="_blank" href="{{$stand->instagram}}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                                    @endif
                                                    @if($stand->linkedin)
                                                    <li><a target="_blank" href="{{$stand->linkedin}}"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                                                    @endif
                                                    @if($stand->skype)
                                                    <li><a target="_blank" href="skype:{{$stand->skype}}?userinfo"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                                                    @endif
                                                </ul>
                                            </div>
                                            <form method="POST"  action="/contact-presenter">
                                                @csrf
                                                 <input type="text" name="id_stand"  id="id_stand" hidden value="{{$stand->id}}">

                                                <div class="form-group">
                                                    <input type="text" name="ime_prezime" class="form-control csiname" id="csiname" placeholder="Ime i Prezime" required>
                                                </div>

                                                <div class="form-group">
                                                    <input type="email" name="mail" class="form-control csiemail" id="csiemail" placeholder="E-mail" required>
                                                </div>

                                                <div class="form-group">
                                                    <input type="text" name="telefon" class="form-control csiemail" id="telefon" placeholder="Telefon" >
                                                </div>

                                                <div class="form-group">
                                                    <textarea class="form-control csimessage" name="poruka" id="poruka" rows="5" placeholder="Poruka" ></textarea>
                                                </div>
                                                <button type="submit" name="submit" value="contact-form" class="csi-btn hvr-glow hvr-radial-out csisend csi-send">Pošalji  <i class="fa fa-paper-plane"></i></button>
                                            </form>

                                        </div> <!--//.COL-->
                                    </div>
                                </footer>
                            </article>
                        </div>
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section> <!--//.NEWS-->
    @else

    <!--NEWS-->
    <section>
        <div id="csi-news" class="csi-news csi-news-singlepage">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <article>
                                <header>
                                    
                                    <div class="text-area">
                                        <h1 class="title">ČEKA ODOBRENJE ADMINISTRATORA</h1>
                                        <div class="hits-area">
                                            <div class="date">
                                                <P>PROCES VERIFIKACIJE TRAJE DO 24 ČASA.</P>
                                            </div>
                                        </div>



                                    </div>
                                </header>

                             
                            </article>
                        </div>
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section> <!--//.NEWS-->
    @endif


@stop