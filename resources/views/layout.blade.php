<!doctype html>
<html class="no-js" lang="en">


<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- The above 3 meta tags *must* come first in the head -->

    <!-- SITE TITLE -->
    <title>Sajam - @yield('site_title')</title>
   

    <!--  FAVICON AND TOUCH ICONS -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('favicon.ico')}}" />  <!-- this icon shows in browser toolbar -->
    <link rel="icon" type="image/x-icon" href="{{asset('favicon.ico')}}" /> <!-- this icon shows in browser toolbar -->

    <link rel="manifest" href="{{asset('favicon.ico')}}">

    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="{{asset('assets/libs/bootstrap/css/bootstrap.min.css')}}" media="all" />

    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="{{asset('assets/libs/fontawesome/css/font-awesome.min.css')}}" media="all"/>


    <!-- GOOGLE FONT -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Oswald:300,400,500,600,700%7cLato:300,400,700"/>


    <!-- OWL CAROUSEL CSS -->
    <link rel="stylesheet" href="{{asset('assets/libs/owlcarousel/owl.carousel.min.css')}}" media="all" />
    <link rel="stylesheet" href="{{asset('assets/libs/owlcarousel/owl.theme.default.min.css')}}" media="all" />


    <!-- POPUP-->
    <link rel="stylesheet" href="{{asset('assets/libs/maginificpopup/magnific-popup.css')}}" media="all"/>


    <!-- MASTER  STYLESHEET  -->
    <link id="csi-master-style" rel="stylesheet" href="{{asset('assets/css/style-default.min.css')}}" media="all" />

    <!-- MODERNIZER CSS  -->
    <script src="{{asset('assets/js/vendor/modernizr-2.8.3.min.js')}}"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-VNMY61KLR0"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-VNMY61KLR0');
    </script>
</head>
<body class="@yield('body')">



<div class="csi-container ">


    <!--HEADER-->
    <header>
        <div id="csi-header" class="csi-header csi-banner-header">
            <div class="csi-header-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <nav class="navbar navbar-default csi-navbar">
                                <div class="csicontainer">
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                                data-target=".navbar-collapse">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        <div class="csi-logo">
                                            <a href="/">
                                                <img src="{{asset('assets/img/logo.png')}}" alt="Logo"/>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="collapse navbar-collapse">
                                        <ul class="nav navbar-nav csi-nav">
                                            <li><a class="csi-scroll" href="/">Naslovna</a></li>
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kategorije <span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="/kategorija/1">IKT</a></li>
                                                    <li><a href="/kategorija/2">Prehrambena Industrija</a></li>
                                                    <li><a href="/kategorija/3">Proizvodnja</a></li>
                                                    <li><a href="/kategorija/4">Usluge</a></li>
                                                    <li><a href="/kategorija/5">Ostalo</a></li>
                                                </ul>
                                            </li>
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">O sajmu <span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="/agenda">Program Sajma</a></li>
                                                    <li><a href="/o-nama">O Organizatoru</a></li>
                                                </ul>
                                            </li>
                                            <!--
                                            <li><a class="csi-scroll" href="/agenda">Program Sajma</a></li>
                                            <li><a class="csi-scroll" href="/o-nama">O Organizatoru</a></li>
                                        -->
                                            <li><a class="csi-scroll" href="/kontakt">Kontakt</a></li>
                                            <li><a target="_blank" class="csi-scroll" href="https://docs.google.com/forms/d/e/1FAIpQLSfa-weccYqqvTi8_6_-c9Zs39_NX5kfZP6Lf7ZJFKsAufUz3A/viewform">Za posetioce</a></li>
                                            @if(Auth::user())
                                                @if(Auth::user()->role == 'ADMIN')
                                                <li><a class="csi-scroll csi-btn" href="/admin-lista">ZAHTEVI</a></li>
                                                @else
                                                <li><a class="csi-scroll csi-btn" href="/izmeni-stand">Moj Nalog</a></li>
                                                @endif
                                            @else
                                                <li><a class="csi-scroll csi-btn" href="/registracija">Postani izlagač </a></li>

                                            @endif
                                            @if(Auth::user())
                                                <li><a class="csi-scroll" href="/logout">Logout</a></li>  
                                            @else
                                                <li><a class="csi-scroll" href="/prijava">Prijavi Se</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                    <!--/.nav-collapse -->
                                </div>
                            </nav>
                        </div>
                    </div>
                    <!--//.ROW-->
                </div>
                <!-- //.CONTAINER -->
            </div>
            <!-- //.INNER-->
        </div>
    </header>
    <!--HEADER END-->

    <title>@yield('title')</title>

    @yield('content')

     <!--FOOTER-->
    <footer>
        <div id="csi-footer" class="csi-footer">
            <div class="csi-inner-bg">
                <div class="csi-inner">
                    <div class="csi-footer-middle">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <div class="csi-footer-logo">
                                        <a class="logo" href="/"><img src="{{asset('assets/img/logo.png')}}" alt="Logo"></a>

                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <div class="csi-footer-single-area">
                                        <div class="csi-footer-single">
                                            <h3 class="footer-title">Kontaktirajte Nas </h3>
                                            <address>
                                                <a class="map-link" target="_blank" href="https://pfm.rs/"><i class="fa fa-envelope" aria-hidden="true"></i>www.pfm.rs</a>
                                                <br>
                                                <a class="map-link" href="mailto:pr@pfm.rs"><i class="fa fa-envelope" aria-hidden="true"></i>pr@pfm.rs</a>
                                            </address>
                                            <a class="map-link" href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> Kneza Miloša 47</a>
                                        </div>
                                        <div class="csi-footer-single">
                                            <h3 class="footer-title">DRUŠTVENE MREŽE</h3>
                                            <ul class="list-inline csi-social">
                                                <li><a target="_blank" href="https://www.facebook.com/privredniforum"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a target="_blank" href="https://www.linkedin.com/company/privredniforum"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                                                <li><a target="_blank" href="https://www.instagram.com/privredni_forum_mladih/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="csi-copyright">
                                        <p class="text">© 2020 Sajam omladinskog preduzetništva ’20, sva prava zadržana</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- //.CONTAINER -->
                    </div>
                </div>
            </div>
            <!-- //.footer Middle -->
        </div>
    </footer>
    <!--FOOTER END-->
</div> <!--//.csi SITE CONTAINER-->


<!-- JQUERY  -->
<script src="{{asset('assets/js/vendor/jquery-1.12.4.min.js')}}"></script>

<!-- BOOTSTRAP JS  -->
<script src="{{asset('assets/libs/bootstrap/js/bootstrap.min.js')}}"></script>


<!-- SKILLS SCRIPT  -->
<script src="{{asset('assets/libs/jquery.validate.js')}}"></script>


<!-- CUSTOM GOOGLE MAP -->
<script type="text/javascript" src="{{asset('assets/libs/gmap/jquery.googlemap.js')}}"></script>

<!-- Owl Carousel  -->
<script src="{{asset('assets/libs/owlcarousel/owl.carousel.min.js')}}"></script>


<!-- type js -->
<script src="{{asset('assets/libs/typed/typed.min.js')}}"></script>


<!-- COUNTDOWN   -->
<script src="{{asset('assets/libs/countdown.js')}}"></script>


<!-- SMOTH SCROLL -->
<script src="{{asset('assets/libs/jquery.smooth-scroll.min.js')}}"></script>
<script src="{{asset('assets/libs/jquery.easing.min.js')}}"></script>


<!-- adding magnific popup js library -->
<script type="text/javascript" src="{{asset('assets/libs/maginificpopup/jquery.magnific-popup.min.js')}}"></script>

<!-- SMOTH SCROLL -->
<script src="{{asset('assets/libs/jquery.smooth-scroll.min.js')}}"></script>
<script src="{{asset('assets/libs/jquery.easing.min.js')}}"></script>


<!-- CUSTOM SCRIPT  -->
<script src="{{asset('assets/js/custom.script.js')}}"></script>


</body>

</html>
