@extends('layout')

@section('body')
    page page-template
@stop

@section('title')
    Prijavi Se
@stop

@section('site_title')
    Prijavi Se
@stop

@section('content')
    @include('message-block')
    <section>
        <div class="csi-banner csi-banner-inner">
            <div class="csi-banner-style">
                <div class="csi-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="csi-heading-area">
                                    <div class="csi-heading">
                                        <h2 class="title">Prijavi Se</h2>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li><a href="/"><i class="icon-home6"></i>Naslovna</a></li>
                                        <li class="active">Prijavi Se</li>
                                    </ul>
                                </div>
                            </div>
                        </div><!--//.ROW-->
                    </div>
                </div>
                <!-- //.INNER -->
            </div>
        </div>
    </section>
    <!--Banner END-->





    <section>
        <div id="csi-contact" class="csi-contact">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-8">
                            <form method="POST" action="/prijava-korisnik">
                                @csrf
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }} ">
                                    <input type="email" name="email" class="form-control csiemail" id="email" placeholder="Email adresa *" value="{{ Request::old('email') }}" required>
                                </div>
                                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }} ">
                                    <input type="password" name="password" class="form-control csiname" id="password" placeholder="Lozinka *" value="{{ Request::old('password') }}" required>
                                </div>
                                <button type="submit" name="submit" value="contact-form" class="csi-btn hvr-glow hvr-radial-out csisend csi-send">Prijavi Se </button>
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                            </form>
                            <!-- MODAL SECTION -->
                            
                        </div> <!--//.COL-->
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section>
@stop