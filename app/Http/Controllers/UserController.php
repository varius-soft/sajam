<?php

namespace App\Http\Controllers;

use App\Category;
use App\Stand;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Mail;
use Carbon\Carbon;


class UserController extends Controller
{
    public function createUser(Request $request)
    {
        

        $email = $request['email'];
        $name = $request['name'];
        $password = bcrypt($request['password']);
        $preduzece = $request['preduzece'];
        $skraceno = $request['skraceno'];
        $pib = $request['pib'];
        $kategorija = $request['kategorija'];

        $user = new User();
        $user->email = $email;
        $user->name = $name;
        $user->password = $password;
        $user->save();

    

        $stand = new Stand();
        $stand->name = $preduzece;
        $stand->email = $email;
        $stand->short_name = $skraceno;
        $stand->pib = $pib;
        $stand->id_category = $kategorija;
        $stand->id_user = $user->id;
        $stand->save();

        $vreme = Carbon::now();

        $vreme->tz='Europe/Belgrade';

        $data =[
           'name' => $name,
           'preduzece' =>  $preduzece,
           'vreme' => $vreme

        ];



        //return view('mail.registracija',compact('name','preduzece','vreme'));
         Mail::send('mail.registracija', $data, function($message) use ($name,$preduzece) {
         $message->to('pr@pfm.rs', 'NOVA REGISTRACIJA - '.$preduzece)->subject('NOVA REGISTRACIJA - '.$preduzece);
         $message->from('sajam@pfm.rs' , 'NOVA REGISTRACIJA - '.$preduzece);
        });

        Auth::attempt(['email' => $request['email'], 'password' => $request['password']]);

        return redirect('/izmeni-stand');
    }

    public function updateUser(Request $request)
    {

      
        Auth::user();


        $user = User::find($request['id']);
        if($user->id == Auth::user()->id  || Auth::user()->role == 'ADMIN')
        {
        $user->email = $request['email'];
        $user->name = $request['name'];
        $user->update();


        $standID = Stand::getStandForUser($user->id); 
        $stand = Stand::find($standID->id);
        $stand->name = $request['preduzece'];
        $stand->email = $request['email'];
        $stand->short_name = $request['skraceno'];
        $stand->id_category = $request['kategorija'];
        $stand->pib = $request['pib'];
        $stand->phone = $request['telefon'];
        $stand->site = $request['sajt'];
        $stand->skype = $request['ss'];
        $stand->facebook = $request['facebook'];
        $stand->instagram = $request['instagram'];
        $stand->linkedin = $request['linkedin'];
        $stand->twiter = $request['tt'];
        $stand->description = $request['description'];
        $stand->period = $request['period'];
        $stand->meeting = $request['meeting'];


        if($request->hasFile('slika'))
        {
            // Get filename with the extension
            $filenameWithExt = $request->file('slika')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('slika')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            //$path = $request->file('slika')->storeAs('public/cover_images/', $fileNameToStore);


            $storagePath = Storage::disk('public_uploads')->put('/'.$stand->id.'/', $request->file('slika'));

            
            /* make thumbnails
            $thumbStore = 'thumb.'.$filename.'_'.time().'.'.$extension;
            $thumb = Image::make($request->file('cover_image')->getRealPath());
            $thumb->resize(80, 80);
            $thumb->save('storage/cover_images/'.$thumbStore);*/
            $stand->image = basename($storagePath);

        }

        
        if($request->hasFile('dokument'))
        {
            // Get filename with the extension
            $filenameWithExt = $request->file('dokument')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('dokument')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'.'.$extension;
            // Upload Image
            //$path = $request->file('slika')->storeAs('public/cover_images/', $fileNameToStore);


            $storagePath = Storage::disk('public_uploads')->put('/'.$stand->id.'/', $request->file('dokument'));

            
            /* make thumbnails
            $thumbStore = 'thumb.'.$filename.'_'.time().'.'.$extension;
            $thumb = Image::make($request->file('cover_image')->getRealPath());
            $thumb->resize(80, 80);
            $thumb->save('storage/cover_images/'.$thumbStore);*/
            $stand->document = basename($storagePath);

        }


        $stand->update();



        }
        else
        {
            return view('greska');
        }
       


        return redirect('/izmeni-stand');
    }

    public function logoutUser()
    {
        Auth::logout();
        return redirect('/');
    }


    public function loginUser(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        if(Auth::attempt(['email' => $request['email'], 'password' => $request['password']]))
        {

            if(Auth::user()->role== 'ADMIN')
            {
                return redirect('/admin-lista');
            }

            return redirect('/izmeni-stand');
        }
        return redirect()->back();
    }


    public function createAdminUser(Request $request)
    {
        

        $email = $request['email'];
        $name = $request['name'];
        $password = bcrypt($request['password']);
        

        $user = new User();
        $user->email = $email;
        $user->name = $name;
        $user->password = $password;
        $user->status = 'APPROVED';
        $user->save();


        return redirect('/');
    }
    
}
