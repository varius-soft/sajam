<?php

namespace App\Http\Controllers;

use App\User;
use App\Stand;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mail;
use Carbon\Carbon;

class StandController extends Controller
{

    public function getStand($id)
    {
        $stand = Stand::getStand($id);



        $category = Category::getCategory($stand->id_category);
        if($stand)
        {
            $admin = 0;
             
            if(Auth::user())
            { 
                if(Auth::user()->role == 'ADMIN')
                {
                     $admin=1;
                }
            }

            $user = User::find($stand->id_user);
            return view('stand', compact('stand', 'category','user','admin'));
            
            
        }
        else
        {
            return view('greska');
        }    
    }

    public function getStandUpdate()
    {

        $user = Auth::user();
        
        if($user)
        {   
            $categories = Category::getAll();
            $stand = Stand::getStandForUser($user->id);
            return view('izmeni-stand', compact('stand', 'user', 'categories'));
        }
        else
        {
            return view('greska');
        }    
       
    }


    public function contactPresenter(Request $request)
    {

        
        //$request['captcha'] = $this->captchaCheck();

        $id_stand = $_POST['id_stand'];
        $stand = Stand::getStand($id_stand);

        $ime_prezime = $_POST['ime_prezime'];
        $telefon = $_POST['telefon'];
        $mail = $_POST['mail'];
        $poruka = $_POST['poruka'];
        $vreme = Carbon::now();

        $vreme->tz='Europe/Belgrade';

        $mailTo = $stand->email;

         $data =[
           'ime_prezime' => $ime_prezime,
           'telefon' =>  $telefon,
           'poruka' => $poruka,
           'mail' => $mail,
           'vreme' => $vreme

        ];

        //return view('mail.kontakt',compact('vreme','mail','poruka','telefon','ime_prezime'));
        //-----------------slanje maila-----------
        
         Mail::send('mail.kontakt', $data, function($message) use ($ime_prezime,$mail,$mailTo) {
         $message->to($mailTo, 'ONLINE SAJAM - '.$ime_prezime)->subject('ONLINE SAJAM - '.$ime_prezime);
         $message->from('sajam@pfm.rs' ,'ONLINE SAJAM - '.$ime_prezime);
        });
    
        //dd($data);


        

     return redirect('/kontakt-uspesan');
    }



    public function contactMain(Request $request)
    {

        
        //$request['captcha'] = $this->captchaCheck();


        $ime_prezime = $_POST['ime_prezime'];
        $telefon = $_POST['telefon'];
        $mail = $_POST['mail'];
        $poruka = $_POST['poruka'];
        $vreme = Carbon::now();

        $vreme->tz='Europe/Belgrade';


         $data =[
           'ime_prezime' => $ime_prezime,
           'telefon' =>  $telefon,
           'poruka' => $poruka,
           'mail' => $mail,
           'vreme' => $vreme

        ];


        // return view('mail.kontakt' ,compact('ime_prezime','telefon','vreme','mail','poruka'));
        //-----------------slanje maila-----------
        
         Mail::send('mail.kontakt', $data, function($message) use ($ime_prezime,$mail) {
         $message->to('pr@pfm.rs', 'ONLINE SAJAM - '.$ime_prezime)->subject('ONLINE SAJAM - '.$ime_prezime);
         $message->from('sajam@pfm.rs' ,'ONLINE SAJAM - '.$ime_prezime);
        });
    
        //dd($data);


        //return view('mailovi.kontakt',compact('vreme','mail','poruka','telefon','ime_prezime'));

     return redirect('/kontakt-uspesan');
    }

    public function getAdmin()
    {
      $stands = Stand::all();
      return view('admin',compact('stands'));
    }


    public function backToNew($id)
    {
      $stand = Stand::find($id);
      $stand->status = 'NEW';
      $stand->save();

      return redirect('/stand/'.$stand->id);
    }

    public function approve($id)
    {
      $stand = Stand::find($id);
      $stand->status = 'APPROVED';
      $stand->save();

      return redirect('/stand/'.$stand->id);
    }

    public function reject($id)
    {
      $stand = Stand::find($id);
      $stand->status = 'BANNED';
      $stand->save();

      return redirect('/stand/'.$stand->id);
    }

  
}
