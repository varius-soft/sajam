<?php

namespace App\Http\Controllers;

use App\Stand;
use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function getWelcome()
    {
        $categories = Category::getAll();

        foreach ($categories as $c) 
        {
            $c->stands = Stand::getForCategory($c->id);
        }

       	return view('naslovna', compact('categories'));
    }

    public function getCategory($id)
    {
        $category = Category::getCategory($id);

        $stands = Stand::getForCategory($id);

        return view('/kategorija', compact('category', 'stands'));
    }

    public function registrationForm()
    {
    	$categories = Category::getAll();
    	return view('registracija', compact('categories'));
    }
}
