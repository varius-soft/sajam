<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{

    protected $table = 'category';

    public function stands()
    {
        return $this->hasMany('App\Stand');
    }


    public static function getAll()
    {
        return DB::table('category')->where('deleted',0)->get();
    }

    public static function getDeleted()
    {
        return DB::table('category')->where('deleted',1)->get();
    }

    public static function restoreCategory($category)
    {
        DB::table('category')->where('id', $category->id)->update(['deleted' => 0]);
    }   

    public static function deleteCategory($category)
    {
        DB::table('category')->where('id', $category->id)->update(['deleted' => 1]);
    } 

    public static function getCategory($id)
    {
       return DB::table('category')->where('id', $id)->first();
    }
}
