<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class User extends Model implements Authenticatable
{
    use \Illuminate\Auth\Authenticatable;

    protected $fillable = ['name','email', 'email_verified_at',  'password', 'remember_token', 'role', 'status', 'deleted' ,'updated_at' , 'creted_at'];   


    public function stands()
    {
        return $this->hasMany('App\Stand');
    }




    public function getAll()
    {
        return DB::table('users')->where('deleted',0)->get();
    }

    public function getDeleted()
    {
        return DB::table('users')->where('deleted',1)->get();
    }

    public function getNew()
    {
        return DB::table('users')->where([['deleted',0], ['status',"NEW"]])->get();
    }

    public function getRejected()
    {
        return DB::table('users')->where([['deleted',0], ['status',"REJECTED"]])->get();
    }

    public function getApproved()
    {
        return DB::table('users')->where([['deleted',0], ['status',"APPROVED"]])->get();
    }

    public function getBanned()
    {
        return DB::table('users')->where([['deleted',0], ['status',"BANNED"]])->get();
    }

    public function restoreUser($user)
    {
         DB::table('users')->where('id', $user->id)->update(['deleted' => 0]);
    }   

    public function deleteUser($user)
    {
         DB::table('users')->where('id', $user->id)->update(['deleted' => 1]);
    } 

    public static function getUser($id)
    {
       return DB::table('users')->where('id', $id)->first();
    }
}
