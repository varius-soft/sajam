<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Stand extends Model
{

    protected $table = 'stand';

    protected $fillable = ['id_user', 'id_category', 'name','pib', 'short_name',  'description', 'phone', 'image', 'site',  'skype', 'deleted' ,'updated_at' , 'created_at' ,'facebook','instagram','twiter','linkedin','email','period','meeting'];   
          


   public function user()
   {
       return $this->belongsTo('App\User');
   }

    public function category()
    {
       return $this->belongsTo('App\Category');
    }  


   public static function getForCategory($id_cat)
    {
        return DB::table('stand')->where('id_category', $id_cat)->get();
    }

    public static function getStand($id)
    {
       return DB::table('stand')->where('id', $id)->first();
    }

    public static function getStandForUser($id_user)
    {
       return DB::table('stand')->where('id_user', $id_user)->first();


    }

}
