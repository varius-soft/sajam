<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/update-user', 'UserController@updateUser');

Route::get('/', 'CategoryController@getWelcome');

Route::get('/kategorija/{id}', 'CategoryController@getCategory');

Route::post('/create-user/', 'UserController@createUser');

//------- ovo koristimo samo pri pravljenju admin     ---//
Route::get('/crt-admin-user',  function () { return view('admin_user'); });
Route::post('/crt-admin-user-function', 'UserController@createAdminUser'); 
//------- ovo koristimo samo pri pravljenju admin     ---//

Route::get('/logout', 'UserController@logoutUser');



Route::post('/contact-presenter', 'StandController@contactPresenter');

Route::post('/contact-main', 'StandController@contactMain');

Route::get('/stand/{id}', 'StandController@getStand');

Route::get('/izmeni-stand', 'StandController@getStandUpdate');

Route::get('/o-nama', function () {
    return view('o-nama');
});

Route::get('/kontakt-uspesan', function () {
    return view('kontakt_uspesan');
});

Route::get('/kontakt', function () {
    return view('kontakt');
});

Route::get('/agenda', function () {
    return view('agenda');
});

Route::get('/registracija', 'CategoryController@registrationForm');

Route::get('/prijava', function () {
    return view('prijava');
});

Route::post('/prijava-korisnik', 'UserController@loginUser');



Route::get('/admin-lista', 'StandController@getAdmin');

Route::get('/vrati-zehtevi/{id}', 'StandController@backToNew');

Route::get('/odobri/{id}', 'StandController@approve');

Route::get('/odbijenje/{id}', 'StandController@reject');



